//
//  RCTConvert+RNPStatus
//  ReactNativePermissions
//
//  Created by Yonah Forst on 23/03/16.
//  Copyright © 2016 Yonah Forst. All rights reserved.
//
//  Adapted For RIMBOS by DIXTRA (dixtra.co)
//  Nicolas Castellanos <nicolas.castellanos@dixtra.co>
//

#import "RCTConvert.h"

static NSString* RNPStatusUndetermined = @"undetermined";
static NSString* RNPStatusDenied = @"denied";
static NSString* RNPStatusAuthorized = @"authorized";
static NSString* RNPStatusRestricted = @"restricted";


typedef NS_ENUM(NSInteger, RNPType) {
    RNPTypeUnknown,
    RNPTypeCamera,
    RNPTypeMicrophone,
    RNPTypePhoto,
    RNPTypeNotification,
    RNPTypeBackgroundRefresh
};

@interface RCTConvert (RNPStatus)

@end
