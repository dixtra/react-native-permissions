//
//  RCTConvert+RNPermissionsStatus.m
//  ReactNativePermissions
//
//  Created by Yonah Forst on 23/03/16.
//  Copyright © 2016 Yonah Forst. All rights reserved.
//
//  Adapted For RIMBOS by DIXTRA (dixtra.co)
//  Nicolas Castellanos <nicolas.castellanos@dixtra.co>
//

#import "RCTConvert+RNPStatus.h"

@implementation RCTConvert (RNPStatus)


RCT_ENUM_CONVERTER(RNPType, (@{ @"camera" : @(RNPTypeCamera),
                                @"microphone" : @(RNPTypeMicrophone),
                                @"photo" : @(RNPTypePhoto),
                                @"notification" : @(RNPTypeNotification),
                                @"backgroundRefresh": @(RNPTypeBackgroundRefresh)
                                }),
                                RNPTypeUnknown, integerValue)

@end
