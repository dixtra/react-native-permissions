//
//  ReactNativePermissions.h
//  ReactNativePermissions
//
//  Created by Yonah Forst on 18/02/16.
//  Copyright © 2016 Yonah Forst. All rights reserved.
//
//  Adapted For RIMBOS by DIXTRA (dixtra.co)
//  Nicolas Castellanos <nicolas.castellanos@dixtra.co>
//

#import "RCTBridgeModule.h"

#import <Foundation/Foundation.h>

@interface ReactNativePermissions : NSObject <RCTBridgeModule>


@end
